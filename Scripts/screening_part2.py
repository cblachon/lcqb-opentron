from opentrons import protocol_api
from opentrons import types

# Test command line:
# python -m opentrons.simulate SCRIPT --custom-labware-path="PATH"

# metadata
metadata = {
    "protocolName": "Screening of complemented PRK mutant - PART 2",
    "author": "Name <clemence.blachon@sorbonne-universite.fr>",
    "description": "Protocol to perform serial dilutions and spot test.",
    "apiLevel": "2.9"}

amplitude = 0.8


def generate_well_list(x, y, col_name, row_name):
    # x étant le nombre de colonnes et y le nombre de lignes
    well_list = list()
    for i in range(x):
        for j in range(y):
            well_list.append(str(row_name[j])+str(col_name[i]))
    return well_list


def shake(pipette, _target, z_offset):
    pipette.move_to(_target.top().move(
        types.Point(x=amplitude, y=0, z=z_offset)))
    pipette.move_to(_target.top().move(
        types.Point(x=-amplitude, y=0, z=z_offset)))
    pipette.move_to(_target.top().move(
        types.Point(x=amplitude, y=0, z=z_offset)))
    pipette.move_to(_target.top().move(
        types.Point(x=-amplitude, y=0, z=z_offset)))


def run(protocol: protocol_api.ProtocolContext):
    # Installation
    # Labware
    nb_tips = 0
    p300_tip_slots = [10, 11]
    p300_tip_rack = [protocol.load_labware(
        "opentrons_96_filtertiprack_200ul", slot) for slot in p300_tip_slots]
    well_slots = [4, 5, 6, 8]
    # changer labware par vwr_96_wellplate_200ul
    well_plates = [protocol.load_labware(
        "vwr_96_wellplate_200ul", slot) for slot in well_slots]
    spot_slots = [1, 2, 3, 9]
    spot_plates = [protocol.load_labware(
        "greiner_96_wellplate_20ul", slot) for slot in spot_slots]
    reservoir = protocol.load_labware("nest_12_reservoir_15ml", 7)

    # Instruments
    p300 = protocol.load_instrument(
        "p300_multi_gen2", "right", tip_racks=p300_tip_rack)

    # Data
    nb_dil = 3
    water_pos, water_vol = "A1", 180
    dil_col = [["A1", "A2", "A3", "A4"],
               ["A5", "A6", "A7", "A8"],
               ["A9", "A10", "A11", "A12"]]
    # spot_col = ["A1", "A2", "A3", "A4", "A5", "A6",
    #             "A7", "A8", "A9", "A10", "A11", "A12"]

    # Protocol
    protocol.home()
    protocol.comment(
        "PART 2a: Dilutions.\nYou can wait before putting your spot plates so "
        "it stays dry.")

    # Dilutions
    # Water transfering
    p300.pick_up_tip()
    nb_tips += 1
    water_transfered = 0
    print("Water - Nb tips:", nb_tips)
    for well_id in range(len(well_plates)):
        for dil in range(1, nb_dil+1):
            for col in range(len(dil_col)):
                p300.aspirate(water_vol, reservoir[water_pos])
                p300.dispense(
                    water_vol, well_plates[well_id][dil_col[col][dil]])
                water_transfered += water_vol
                if water_transfered >= 15000-180:
                    water_pos = "A2"

    shake(p300, well_plates[-1]["A12"], -1)

    # p300.drop_tip()

    # Transfo transfering
    dil_vol = 20
    for well_id in range(len(well_plates)):
        for col in dil_col:
            if not p300.has_tip:
                p300.pick_up_tip()
                nb_tips += 1
                print("Transfo - Nb tips:", nb_tips)
            for dil in range(len(col)-1):
                p300.mix(5, 100, well_plates[well_id][col[dil]])
                p300.aspirate(dil_vol, well_plates[well_id][col[dil]])
                p300.dispense(dil_vol, well_plates[well_id][col[dil+1]])
                shake(p300, well_plates[well_id][col[dil+1]], -1)
            p300.drop_tip()

    # Spotting step
    protocol.home()
    protocol.pause("PART 2b: Spottings.\nPlease make sure your spot plates are"
                   " located in the following slots: {}".format(
                    ", ".join(str(x) for x in spot_slots)))
    # Spotting
    for well_id in range(len(well_plates)):
        for dil in reversed(dil_col):
            if not p300.has_tip:
                p300.pick_up_tip()
            nb_tips += 1
            print("Spot - Nb tips:", nb_tips)
            for col in reversed(dil):
                p300.mix(5, 100, well_plates[well_id][col])
                p300.aspirate(10, well_plates[well_id][col])
                p300.dispense(10, spot_plates[well_id][col])
            p300.drop_tip()
    protocol.home()
