from opentrons import protocol_api

# metadata
metadata = {
    "protocolName": "Mag deck",
    "author": "Name <clemence.blachon@sorbonne-universite.fr>",
    "description": "Protocol to test magnetic module",
    "apiLevel": "2.9"}


def run(protocol: protocol_api.ProtocolContext):
    # PARAMETERS and DATA
    vol_transfer = 50

    # LABWARE
    mag_deck = protocol.load_module("magnetic module", 1)
    mag_plate = mag_deck.load_labware("biorad_96_wellplate_200ul_pcr")
    source_plate = protocol.load_labware("greiner_96_wellplate_20ul", 2)
    dest_plate = protocol.load_labware("greiner_96_wellplate_20ul", 3)
    tip_slots = [5]
    tip_rack = [protocol.load_labware(
        "opentrons_96_tiprack_300ul", slot) for slot in tip_slots]

    # INSTRUMENTS
    p20 = protocol.load_instrument(
        "p300_multi_gen2", "right", tip_racks=tip_rack)

    # PROTOCOL

    # POUR PERMETTRE DE REMPLIR LE MAG DECK A PARTIR D"UNE PLAQUE 96
    for col in range(len(source_plate.rows()[0])):
        p20.pick_up_tip(tip_rack[0].rows()[0][col])
        p20.aspirate(vol_transfer, source_plate.rows()[0][col])
        p20.dispense(vol_transfer, mag_plate.rows()[0][col])
        p20.drop_tip(tip_rack[0].rows()[0][col])

    # EN FONCTION DU TEMPS TOTAL DE PAUSE, IL VA S"ARRETER X FOIS TOUTES
    # LES MINUTES POUR PERMETTRE A L"UTILISATEUR DE VERIFIER L"ETAT DU
    # MAG DECK ET SON EFFICACITE. IL S"ARRETE A CHAQUE TOUR ET REDEMARRE
    # DES QUE L"UTILISATEUR RELANCE LE PROTOCOL
    total_pause = 5  # minutes
    for i in range(total_pause):
        mag_deck.engage()
        protocol.delay(minutes=1)
        mag_deck.disengage()
        protocol.pause("Check columns status.")

    # POUR PERMETTRE DE TRANSFERER DEPUIS LE MAG DECK  VERS UNE PLAQUE 96
    for col in range(len(mag_plate.rows()[0])):
        p20.pick_up_tip(tip_rack[0].rows()[0][col])
        p20.aspirate(vol_transfer, mag_plate.rows()[0][col])
        p20.dispense(vol_transfer, dest_plate.rows()[0][col])
        p20.drop_tip(tip_rack[0].rows()[0][col])
