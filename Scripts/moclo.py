from opentrons import protocol_api

# Test command line:
# python -m opentrons.simulate SCRIPT --custom-labware-path="PATH"

# metadata
metadata = {
    "protocolName": "MoClo",
    "author": "Name <clemence.blachon@sorbonne-universite.fr>",
    "description": "Protocol to perform MoClo",
    "apiLevel": "2.9"}


def run(protocol: protocol_api.ProtocolContext):
    # PARAMETERS and DATA
    assembly_dict = dict()
    temp_deck = protocol.load_module("Temperature Module", 10)
    temp_deck.set_temperature(4)
    dest_plate = temp_deck.load_labware(
        "opentrons_96_aluminumblock_generic_pcr_strip_200ul", 10)

    # Labware
    bank = protocol.load_labware("corning_384_wellplate_180ul", 1)
    tip_slots = [3, 4, 5, 6, 7, 8]
    tip_rack = [protocol.load_labware(
        "opentrons_96_tiprack_20ul", slot) for slot in tip_slots]
    tube_rack = protocol.load_labware("blocfroid_24_wellplate_1500ul", 9)
    master_mix = "A1"  # WATER + MASTER MIX

    # Instruments
    p20 = protocol.load_instrument(
        "p20_single_gen2", "left", tip_racks=tip_rack)

    # TAMPON T4 TRANSFER
    p20.pick_up_tip()
    for dest, data in assembly_dict.items():
        p20.aspirate(data[-1], tube_rack[master_mix])
        p20.dispense(data[-1], dest_plate[dest])
        data.pop(-1)
    p20.drop_tip()

    for dest, data in assembly_dict.items():
        # BACKBONES and PARTS TRANSFER
        for i in range(0, len(data), 2):
            p20.pick_up_tip()
            p20.mix(3, 15, bank[data[i]])
            p20.aspirate(data[i+1], bank[data[i]])
            p20.dispense(data[i+1], dest_plate[dest])
            p20.drop_tip()

    temp_deck.set_temperature(42)
    protocol.delay(minutes=120)
    temp_deck.set_temperature(80)
    protocol.delay(minutes=20)
    temp_deck.deactivate()
