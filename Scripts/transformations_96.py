from opentrons import protocol_api
from opentrons import types

metadata = {
    "protocolName": "96 Transformations",
    "author": "Clémence Blachon <clemenceblachon@gmail.com>",
    "description": "Transformation protocol using OT2",
    "apiLevel": "2.9"}


def shake(pipette, _target, z_offset, amplitude):
    pipette.move_to(_target.top().move(types.Point(x=amplitude, y=0,
                                                   z=z_offset)))
    pipette.move_to(_target.top().move(types.Point(x=-amplitude, y=0,
                                                   z=z_offset)))
    pipette.move_to(_target.top().move(types.Point(x=amplitude, y=0,
                                                   z=z_offset)))
    pipette.move_to(_target.top().move(types.Point(x=-amplitude, y=0,
                                                   z=z_offset)))


def run(protocol: protocol_api.ProtocolContext):
    # MISE EN PLACE DU LABWARE
    temp_deck = protocol.load_module("temperature module gen2", 10)
    temp_plate = temp_deck.load_labware("opentrons_96_aluminumblock_"
                                        "generic_pcr_strip_200ul", 10)
    lb_reservoir = protocol.load_labware("agilent_4_reservoir_73000ul", 4)
    wellplate_slots = [7, 8]
    dil_plates = [protocol.load_labware("vwr_96_wellplate_200ul", slot)
                  for slot in wellplate_slots]
    tube_rack = protocol.load_labware("biorad_96_wellplate_200ul_pcr", 9)
    spot_slots = [1, 2, 3]
    spot_plates = [protocol.load_labware("sbs_plate", slot)
                   for slot in spot_slots]
    tipracks_slots = [11, 5, 6]
    tiprack_p300 = [protocol.load_labware("opentrons_96_tiprack_300ul", slot)
                    for slot in tipracks_slots]

    # MISE EN PLACE DES PIPETTES
    p300 = protocol.load_instrument("p300_multi_gen2", "right",
                                    tip_racks=tiprack_p300)

    # DATA
    lb_vol = 100
    bacteria_vol = 2.5
    water_vol = 50

    protocol.home()

    temp_deck.set_temperature(4)
    protocol.pause("Mettre les bactéries dans les colonnes suivantes: %s" %
                   (tube_rack.rows()[0]))
    for pos in range(len(tube_rack.rows()[0])):
        p300.pick_up_tip()
        p300.aspirate(bacteria_vol, tube_rack.rows()[0][pos])
        shake(p300,  tube_rack.rows()[0][pos], -1, 0.8)
        p300.dispense(bacteria_vol, temp_plate.rows()[0][pos])
        p300.mix(2, 40, temp_plate.rows()[0][pos])
        shake(p300,  temp_plate.rows()[0][pos], -3, 0.8)
        p300.drop_tip()
    # print("20mn pause")
    # protocol.delay(minutes=20)
    # temp_deck.set_temperature(42)
    # temp_deck.start_set_temperature(4)
    # A TESTER: PAS SÛR QUE ÇA MARCHE
    protocol.pause("Pause pour incubation bactéries à 4°C")
    temp_deck.set_temperature(42)
    temp_deck.await_temperature(42)
    temp_deck.start_set_temperature(6)

    p300.flow_rate.dispense = 400

    p300.pick_up_tip(tiprack_p300[1]["A1"])
    for i in range(0, len(temp_plate.rows()[0]), 3):
        p300.aspirate(lb_vol*3, lb_reservoir["A1"])
        for j in temp_plate.rows()[0][i:i+3]:
            p300.dispense(lb_vol, j)
        shake(p300, j, -3, 0.8)

    for round in range(3):
        if round == 0:
            source_tiprack = 1
        else:
            source_tiprack = 0
        for i in range(len(temp_plate.rows()[0])):
            if not p300.has_tip:
                p300.pick_up_tip(tiprack_p300[source_tiprack].rows()[0][i])
            p300.mix(3, 100, temp_plate.rows()[0][i])
            shake(p300,  temp_plate.rows()[0][i], -3, 0.8)
            p300.drop_tip(tiprack_p300[0].rows()[0][i])
        if round == 0:
            temp_deck.await_temperature(6)
            temp_deck.set_temperature(37)
        else:
            protocol.delay(minutes=10)

    protocol.pause("Étape de spotting:\n"
                   "Mettre les plaques de spottings dans les bons slots ({})."
                   .format(spot_slots))
    p300.pick_up_tip(tiprack_p300[2]["A1"])
    for round in range(2):
        for i in range(0, len(dil_plates[round].rows()[0]), 3):
            nb_col = len(dil_plates[round].rows()[0][i:i+3])
            p300.aspirate(water_vol*nb_col, lb_reservoir["A1"])
            for j in dil_plates[round].rows()[0][i:i+3]:
                p300.dispense(water_vol, j)
            shake(p300, j, -3, 0.8)
    p300.drop_tip(tiprack_p300[2]["A1"])
    tiprack_p300[1].reset()

    dil_idx = 0
    dil_plt_idx = 0
    spot_idx = 0
    spot_plate_idx = 0
    for i in range(len(temp_plate.rows()[0])):
        source = temp_plate.rows()[0][i]
        p300.pick_up_tip(tiprack_p300[2].rows()[0][i])
        spot_well = spot_plates[spot_plate_idx].rows()[0][spot_idx]
        p300.mix(3, 50, source)
        p300.aspirate(60, source)
        p300.dispense(10, spot_well)
        protocol.delay(seconds=2)
        p300.move_to(spot_well.bottom().move(types.Point(x=0, y=0, z=2)))
        protocol.delay(seconds=2)
        p300.move_to(spot_well.bottom().move(types.Point(x=0, y=0, z=-1)))
        protocol.delay(seconds=2)
        spot_idx += 1
        for dil in range(2):
            if dil == 0:
                vol_asp = 60
            else:
                vol_asp = 20
            p300.dispense(50, dil_plates[dil_plt_idx].rows()[0][dil_idx])
            p300.mix(3, 50, dil_plates[dil_plt_idx].rows()[0][dil_idx])
            spot_well = spot_plates[spot_plate_idx].rows()[0][spot_idx]
            p300.aspirate(vol_asp, dil_plates[dil_plt_idx].rows()[0][dil_idx])
            p300.dispense(10, spot_well)
            protocol.delay(seconds=2)
            p300.move_to(spot_well.bottom().move(types.Point(x=0, y=0, z=2)))
            protocol.delay(seconds=2)
            p300.move_to(spot_well.bottom().move(types.Point(x=0, y=0, z=-1)))
            protocol.delay(seconds=2)
            dil_idx += 1
            spot_idx += 1
        if spot_idx >= 12:
            spot_idx = 0
            spot_plate_idx += 1
        if dil_idx >= 12:
            dil_idx = 0
            dil_plt_idx += 1
        p300.drop_tip()
