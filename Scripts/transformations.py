from opentrons import protocol_api
import math

# metadata
metadata = {
    "protocolName": "Transformation",
    "author": "Clémence Blachon <clemenceblachon@gmail.com>",
    "description": "Transformation protocol using OT2",
    "apiLevel": "2.9"}


def run(protocol: protocol_api.ProtocolContext):

    # MISE EN PLACE DU LABWARE
    temp_deck = protocol.load_module("Temperature Module", 10)
    temp_plate = temp_deck.load_labware(
        "opentrons_96_aluminumblock_generic_pcr_strip_200ul", 10)
    lb_reservoir = protocol.load_labware("nest_12_reservoir_15ml", 4)
    tube_rack = protocol.load_labware(
        "opentrons_24_tuberack_nest_1.5ml_snapcap", 6)
    tiprack_p300 = protocol.load_labware("opentrons_96_tiprack_300ul", 11)
    tiprack_p20 = protocol.load_labware("opentrons_96_tiprack_20ul", 8)
    spot_plate = protocol.load_labware("greiner_96_wellplate_20ul", 2)

    # MISE EN PLACE DES PIPETTES
    p20 = protocol.load_instrument(
        "p20_single_gen2", "left", tip_racks=[tiprack_p20])
    p300 = protocol.load_instrument(
        "p300_multi", "right", tip_racks=[tiprack_p300])

    protocol.home()
    # INPUT DICT, dest_col = (dict, list)
    input_dict, dest_col = dict(), list()
    nb_transfo = 96
    lb_vol = 150
    temp_deck.set_temperature(4)
    protocol.pause(
        "Put the bacteria in the following columns: %s" % (dest_col))
    for pos, data in input_dict.items():
        p20.pick_up_tip()
        p20.aspirate(data[1], tube_rack[pos])
        p20.dispense(data[1], temp_plate[data[0]])
        p20.mix(3, 20, temp_plate[data[0]])
        p20.drop_tip()
    print("20mn pause")
    protocol.delay(minutes=20)
    temp_deck.set_temperature(42)
    temp_deck.start_set_temperature(4)

    for i in dest_col:
        p300.pick_up_tip()
        p300.aspirate(lb_vol, lb_reservoir["A1"])
        p300.dispense(lb_vol, temp_plate[i])
        p300.mix(3, 100)
        p300.drop_tip()

    temp_deck.await_temperature(4)
    print("5mn pause")
    protocol.delay(minutes=5)
    temp_deck.set_temperature(37)
    for i in range(3):
        for i in dest_col:
            if not p300.has_tip:
                p300.pick_up_tip()
            p300.mix(3, lb_vol, temp_plate[i])
            if len(dest_col) > 1:
                p300.drop_tip()
            protocol.home()
        print("10mn pause")
        protocol.delay(minutes=10)
    if p300.has_tip:
        p300.drop_tip()
    temp_deck.deactivate()

    protocol.pause(
        "Spotting steps starting. "
        "Please make sure the plates are at the good positions.")
    nb_dilution = 0
    dilution_factor = 0
    stock_solution = 150/dilution_factor
    water = 150 - stock_solution
    column = [["A1", "A2", "A3", "A4", "A5", "A6"],
              ["A7", "A8", "A9", "A10", "A11", "A12"]]
    for tr in range(0, math.ceil(nb_transfo/8)):
        p300.default_speed = 400
        p300.flow_rate.dispense = 200
        spot_index = 0
        p300.pick_up_tip()
        for i in range(1, nb_dilution):
            p300.aspirate(water, lb_reservoir["A2"])
            p300.dispense(water, temp_plate[column[tr][i]])
        p300.aspirate(40, temp_plate[column[tr][0]])
        p300.dispense(10, spot_plate[column[tr][0]])
        spot_index += 1
        p300.drop_tip()
        for i in range(nb_dilution-1):
            p300.pick_up_tip()
            p300.aspirate(stock_solution, temp_plate[column[tr][i]])
            p300.dispense(stock_solution, temp_plate[column[tr][i+1]])
            p300.mix(3, 70)
            p300.aspirate(40, temp_plate[column[tr][i+1]])
            p300.dispense(10, spot_plate[column[tr][spot_index]])
            spot_index += 1
            p300.drop_tip()
    protocol.home()
