
from opentrons import protocol_api
import numpy as np
import math

# metadata
metadata = {
    "protocolName": "Dilutions + spot test",
    "author": "Name <clemence.blachon@sorbonne-universite.fr>",
    "description": "Protocol to perform dilutions and spot test",
    "apiLevel": "2.9"}


def run(protocol: protocol_api.ProtocolContext):
    # MISE EN PLACE DU LABWARE
    source_plate = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1",
                    "A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2",
                    "A3", "B3", "C3", "D3", "E3", "F3", "G3", "H3",
                    "A4", "B4", "C4", "D4", "E4", "F4", "G4", "H4",
                    "A5", "B5", "C5", "D5", "E5", "F5", "G5", "H5",
                    "A6", "B6", "C6", "D6", "E6", "F6", "G6", "H6",
                    "A7", "B7", "C7", "D7", "E7", "F7", "G7", "H7",
                    "A8", "B8", "C8", "D8", "E8", "F8", "G8", "H8",
                    "A9", "B9", "C9", "D9", "E9", "F9", "G9", "H9",
                    "A10", "B10", "C10", "D10", "E10", "F10", "G10", "H10",
                    "A11", "B11", "C11", "D11", "E11", "F11", "G11", "H11",
                    "A12", "B12", "C12", "D12", "E12", "F12", "G12", "H12"]
    water_reservoir = protocol.load_labware("nest_12_reservoir_15ml", 8)
    tips_slots = [10, 11]
    tiprack_p300 = [protocol.load_labware(
        "opentrons_96_tiprack_300ul", slot) for slot in tips_slots]
    nb_plates = 3
    spot_slots = [1, 2, 3]
    dil_slots = [4, 5, 6]
    source_plate = protocol.load_labware("corning_96_wellplate_360ul_flat", 7)
    all_spot_plate = [protocol.load_labware(
        "greiner_96_wellplate_20ul", slot) for slot in spot_slots[:nb_plates]]
    all_dil_plate = [protocol.load_labware(
        "corning_96_wellplate_360ul_flat", slot)
        for slot in dil_slots[:nb_plates]]

    # MISE EN PLACE DES PIPETTES
    p300 = protocol.load_instrument(
        "p300_multi", "right", tip_racks=tiprack_p300)

    protocol.home()
    nb_col = 12
    nb_dilution = 3
    dilution_factor = 2
    total_volume = 150
    stock_solution = total_volume/dilution_factor
    water = total_volume - stock_solution
    column = ["A1", "A2", "A3", "A4", "A5", "A6",
              "A7", "A8", "A9", "A10", "A11", "A12"]
    dil_column = np.array_split(column, math.ceil(nb_col/nb_plates))
    split_col = np.array_split(column[0:nb_col], nb_plates)

    # ON MET L"EAU DANS TOUS LES PUITS DE DILUTION
    p300.pick_up_tip()
    for slot in range((nb_plates)):
        dil_plate = all_dil_plate[slot]
        spot_plate = all_spot_plate[slot]
        cols = split_col[slot]
        for tr in range(0, len(cols)):
            p300.default_speed = 400
            p300.flow_rate.dispense = 200
            for i in range(1, nb_dilution):
                p300.aspirate(water, water_reservoir["A1"])
                p300.dispense(water, dil_plate[dil_column[tr][i]])
    col_index = 0
    for slot in range(nb_plates):
        dil_plate = all_dil_plate[slot]
        spot_plate = all_spot_plate[slot]
        cols = split_col[slot]
        for tr in range(0, len(cols)):
            if not p300.has_tip:
                p300.pick_up_tip()
            p300.default_speed = 400
            p300.flow_rate.dispense = 200
            for i in range(0, nb_dilution):
                if i == 0:
                    p300.aspirate(stock_solution+water, source_plate[cols[tr]])
                    p300.dispense(stock_solution+water,
                                  dil_plate[dil_column[tr][i]])
                else:
                    p300.aspirate(stock_solution,
                                  dil_plate[dil_column[tr][i-1]])
                    p300.dispense(stock_solution, dil_plate[dil_column[tr][i]])
                p300.mix(3, 70)
                p300.aspirate(10, dil_plate[dil_column[tr][i]])
                p300.dispense(10, spot_plate[dil_column[tr][i]])
            col_index += 1
            p300.drop_tip()
    protocol.home()
