from opentrons import protocol_api
import string

# Test command line:
# python -m opentrons.simulate SCRIPT --custom-labware-path="PATH"

# metadata
metadata = {
    "protocolName": "Aliquots",
    "author": "Name <clemence.blachon@sorbonne-universite.fr>",
    "description": "Protocol to perform aliquots",
    "apiLevel": "2.9"}


def generate_well_list(x, y):
    alphabet = string.ascii_uppercase
    well_list = list()
    for i in range(x):
        for j in range(y):
            well_list.append(alphabet[j]+str(i+1))
    return well_list


def run(protocol: protocol_api.ProtocolContext):
    # PARAMETERS and DATA
    source_wells = ["A1"]
    nb_dest_wells = [10]
    wells = generate_well_list(12, 8)
    vol = [5]

    # Labware
    source_plate = protocol.load_labware("greiner_96_wellplate_20ul", 1)
    dest_plate = protocol.load_labware("greiner_96_wellplate_20ul", 2)
    tip_slots = [3, 4, 5, 6, 7, 8]
    tip_rack = [protocol.load_labware(
        "opentrons_96_tiprack_20ul", slot) for slot in tip_slots]

    # Instruments
    p20 = protocol.load_instrument(
        "p20_single_gen2", "left", tip_racks=tip_rack)

    # Protocol
    protocol.home()
    p20.pick_up_tip()
    for source in range(len(source_wells)):
        wells = wells[0:nb_dest_wells[source]]
        for i in range(nb_dest_wells[source]):
            # print(i, vol[source], source_wells[source], dest_plate[source])
            p20.aspirate(vol[source], source_plate[source_wells[source]])
            p20.dispense(vol[source], dest_plate[wells[i]])
    p20.drop_tip()
    protocol.home()
