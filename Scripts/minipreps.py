# ***********************  Protocol **********************
from opentrons import protocol_api
from opentrons import types
import time
import math
import sys

# Test command line:
# python -m opentrons.simulate SCRIPT --custom-labware-path="PATH"

metadata = {
    "protocolName": "Minipreps",
    "author": "Clémence Blachon",
    "description": "Protocol to do 96 minipreps at a time "
                   "with Macherey-Nagel vacuum.",
    "apiLevel": "2.9"}

user = "clemenceblachon@gmail.com"

# PARAMETERS
# Number of sample must be less than 96.
sample_number = 96
# Number of columns at the left of the vacuum that are dirty
column_offset = 0

# Resuspension parameters
resuspension_mix_vol = 200
resuspension_mix_steps = 14
lysis_time = 330  # seconds

# Lysis parameters
lysis_volume = 250
lysis_mix_vol = 200
lysis_mix_steps = 10

# Neutralisation parameters
n3_mix_steps = 6

# Elution parameters
elution_vol = 80

# ************************ Functions ***************************


def shake(pipette, _target, z_offset):
    """Shake pipette at the top of the target at a given height"""
    amplitude = 0.8
    for x in range(5):
        pipette.move_to(_target.top().move(
            types.Point(x=amplitude, y=0, z=z_offset)))
        pipette.move_to(_target.top().move(
            types.Point(x=-amplitude, y=0, z=z_offset)))


def perform_resuspension(pipette, dna_tips_loc, plate, step):
    """Resuspension, multiple mixing steps for some specific columns"""
    for i in range(0, len(plate), step):
        pipette.pick_up_tip(dna_tips_loc[plate[i]])
        for j in range(resuspension_mix_steps):
            pipette.aspirate(resuspension_mix_vol,
                             plate[i].bottom(3))  # .bottom(1)
            pipette.dispense(resuspension_mix_vol,
                             plate[i].bottom(5))  # .bottom(20)
        shake(pipette, plate[i], -5)
        pipette.flow_rate.aspirate = 150
        pipette.flow_rate.dispense = 300
        for w in range(2):
            pipette.aspirate(200, plate[i+1].bottom(4))
            pipette.dispense(200, plate[i+1].bottom(4))
        shake(pipette, plate[i+1], -5)
        pipette.touch_tip(v_offset=-5, radius=0.9)
        pipette.drop_tip(dna_tips_loc[plate[i]])


def perform_lysis(protocol, pipette, sol_tip_loc, dna_tips_loc,
                  solution_2, plate, step):
    """Lysis, transfering lysis buffer, mixing and washing tips"""
    # Roughly 50s to execute
    pipette.flow_rate.aspirate = 100
    pipette.flow_rate.dispense = 100
    pipette.pick_up_tip(sol_tip_loc)
    for i in range(0, len(plate), step):
        pipette.transfer(lysis_volume, solution_2, plate[i].top(
        ), air_gap=0, new_tip="never")  # add lysis buffer
        shake(pipette, plate[i], 1)
    pipette.drop_tip(sol_tip_loc)
    # Gently mix for some time
    pipette.flow_rate.aspirate = 100
    pipette.flow_rate.dispense = 100
    for i in range(0, len(plate), step):
        pipette.pick_up_tip(dna_tips_loc[plate[i]])
        for j in range(lysis_mix_steps):
            pipette.aspirate(lysis_mix_vol, plate[i].bottom(4))
            pipette.dispense(lysis_mix_vol, plate[i].bottom(16))
        shake(pipette, plate[i], -5)
        protocol.delay(seconds=3)
        pipette.flow_rate.aspirate = 150
        pipette.flow_rate.dispense = 300
        for w in range(2):
            pipette.aspirate(200, plate[i+1].bottom(4))
            pipette.dispense(200, plate[i+1].bottom(4))
        shake(pipette, plate[i+1], -5)
        pipette.touch_tip(v_offset=-5, radius=0.9)
        pipette.drop_tip(dna_tips_loc[plate[i]])


def perform_neutral(protocol, pipette, sol_tip_loc, dna_tips_loc,
                    solution_3, plate, vacuum, step):
    """Neutralisation, transferring neutralisation buffer, mixing
    and washing tips"""
    # Roughly 1mn to execute
    pipette.flow_rate.aspirate = 300
    pipette.flow_rate.dispense = 300
    pipette.pick_up_tip(sol_tip_loc)
    # Transfer of the mix to the 96 plate"s column
    for i in range(0, len(plate), step):
        pipette.aspirate(300, solution_3)
        pipette.dispense(300, plate[i].top())
        shake(pipette, plate[i], 1)
    pipette.aspirate(50*step, solution_3)
    for i in range(0, len(plate), step):
        pipette.dispense(50, plate[i].top())
        shake(pipette, plate[i], 1)
    pipette.drop_tip(sol_tip_loc)
    # Gently mix for some time
    pipette.flow_rate.aspirate = 300
    pipette.flow_rate.dispense = 300
    for i in range(0, len(plate), step):
        pipette.pick_up_tip(dna_tips_loc[plate[i]])
        for j in range(n3_mix_steps):
            pipette.aspirate(200, plate[i].bottom(4))
            pipette.dispense(200, plate[i].bottom(20))
        pipette.flow_rate.aspirate = 200
        pipette.flow_rate.dispense = 300
        pipette.aspirate(150, plate[i].bottom(11))
        pipette.dispense(150, plate[i].bottom(4))
        pipette.aspirate(150, plate[i].bottom(11))
        pipette.dispense(150, plate[i].bottom(4))
        pipette.aspirate(100, plate[i].bottom(11))
        pipette.dispense(100, plate[i].bottom(4))
        tip_position = [4, 4, 4, 4, 2]
        for j in range(5):
            pipette.aspirate(200, plate[i].bottom(tip_position[j]))
            protocol.delay(seconds=2)
            pipette.move_to(plate[i].top().move(types.Point(x=0, y=0, z=-5)))
            protocol.delay(seconds=1)
            shake(pipette, plate[i], -5)
            pipette.touch_tip(v_offset=-5, radius=0.9)
            pipette.dispense(200, vacuum[plate[i]].top().move(
                types.Point(x=0, y=0, z=-5)))
            shake(pipette, vacuum[plate[i]], -5)
            pipette.touch_tip(v_offset=-5, radius=0.75)
        pipette.drop_tip()


def run(protocol: protocol_api.ProtocolContext):
    """Main run function"""
    col_num = math.ceil(sample_number/8)
    if sample_number > 96:
        sys.exit("You have too many samples. The limit is up to 96.")
    if col_num != 0:
        protocol.comment(
            "\nWarning, reagent will be wasted in {} wells\n"
            .format(8 - sample_number % 8))
    if col_num > 12-column_offset:
        sys.exit("You don't have enough free columns to miniprep "
                 "your {} samples.\nYou must have {} free columns, "
                 "and you need {} clean columns."
                 .format(sample_number, 12-column_offset, col_num))
    protocol.comment(
        "\nPerfect, we're going to miniprep {} samples.".format(sample_number))

    # Labware
    _4wells_reservoir = protocol.load_labware("agilent_4_reservoir_73000ul", 9)
    _3wells_reservoir = protocol.load_labware("agilent_3_reservoir_94000ul", 3)
    vacuum = protocol.load_labware("machereynagel_96_wellplate_800ul", 1)
    plate_A = protocol.load_labware("deepwell_mprep", 11)
    plate_B = protocol.load_labware("deepwell_mprep", 5)

    # 4-well reservoir: all volumes indicated in comments
    # correspond to the volumes needed for 96 minipreps with 10% more
    wash_water = _4wells_reservoir["A1"]  # need 60mL
    a2_sol = _4wells_reservoir["A2"]  # need 27mL
    a3_sol = _4wells_reservoir["A3"]  # need 38mL
    elution_buffer = _4wells_reservoir["A4"]  # need 2mL

    # 3-well reservoir: all volumes indicated in comments
    # correspond to the volumes needed for 96 minipreps with 10% more
    aw_sol = _3wells_reservoir["A1"]  # need 75mL
    a4_wash_buffer1 = _3wells_reservoir["A2"]  # need 95mL
    a4_wash_buffer2 = _3wells_reservoir["A3"]  # need 95mL

    # Pipette and tips
    tips_list = ["8", "10", "6"]
    tipracks = [protocol.load_labware(
        "opentrons_96_tiprack_300ul", slot) for slot in tips_list]
    dna_tips = tipracks[0]
    elution_tips = tipracks[1]
    solvent_tips = tipracks[2]

    pipette = protocol.load_instrument("p300_multi_gen2", "right", tipracks)
    pipette.flow_rate.blow_out = 1000

    all_columns = plate_A.rows()[0] + plate_B.rows()[0]
    all_samples = all_columns[:col_num*2]
    vacuum_samples = vacuum.rows()[0][column_offset:col_num+column_offset]

    tips_dest_well, vac_dest_well = dict(), dict()
    for sample in range(0, len(vacuum_samples)):
        vac_dest_well[all_samples[sample*2]] = vacuum_samples[sample]

    for idx in range(0, len(plate_A.rows()[0]), 2):
        tips_dest_well[plate_A.rows()[0][idx]] = dna_tips.rows()[0][idx]
        tips_dest_well[plate_B.rows()[0][idx]] = dna_tips.rows()[0][idx+1]

    pipette.flow_rate.aspirate = 180
    pipette.flow_rate.dispense = 300

    protocol.comment(
        "Filling the destinations plate with water to wash "
        "the tips after lysis step.")
    pipette.pick_up_tip(solvent_tips["A1"])
    for well in range(1, len(all_samples), 2):
        for round in range(2):
            pipette.aspirate(250, wash_water)
            pipette.dispense(250, all_samples[well].top())
            shake(pipette, all_samples[well], 2)
    pipette.drop_tip()
    protocol.home()

    # Nb of columns to treat at each round
    # 2 is recommanded to respect the lysis time
    step = 2
    protocol.pause("Starting lysis and neutralisation steps? "
                   "{} columns in a row are done.".format(step))

    for i in range(0, len(all_samples), step*2):
        # LYSIS STEP: FROM 5 TO 6 MINUTES
        protocol.comment("Resuspension...")
        perform_resuspension(pipette, tips_dest_well,
                             all_samples[i:i+2*step], step)
        start_time = time.perf_counter()
        protocol.comment("Lysis...")
        perform_lysis(protocol,
                      pipette, solvent_tips["A2"], tips_dest_well,
                      a2_sol, all_samples[i:i+2*step], step)
        protocol.delay(lysis_time-(time.perf_counter()-start_time))
        protocol.comment("Neutralisation...")
        perform_neutral(protocol, pipette, solvent_tips["A3"], tips_dest_well,
                        a3_sol, all_samples[i:i+2*step], vac_dest_well, step)
    protocol.home()

    comment = "\tEnd of the lysis and neutralisation steps."
    comment += "\n* Put the binding plate into the manifold."
    comment += "\n* Put the filter plate on top of the manifold lid."
    comment += "\n* Turn on the vacuum pump: -200/-400 mbar (1-5min)"
    comment += "\n* Put the MN wash plate into the manifold."
    comment += "\n* Put the filter plate on top of the manifold lid."
    comment += "\n* Turn on the vacuum pump: -200/-400 mbar (1 min)"
    comment += "\n\nIMPORTANT: DON'T START THE PROTOCOL UNTIL THE "
    comment += "VACUUM IS READY FOR THE NEXT STEPS!"
    protocol.pause(comment)

    protocol.comment("First wash")
    pipette.flow_rate.aspirate = 250
    pipette.flow_rate.dispense = 200
    pipette.pick_up_tip(solvent_tips["A4"])
    for well in vacuum_samples:
        for j in range(3):
            pipette.aspirate(200, aw_sol)
            pipette.move_to(aw_sol.top().move(types.Point(x=0, y=0, z=2)))
            protocol.delay(seconds=1)
            shake(pipette, aw_sol, 2)
            pipette.dispense(200, well.top())
    pipette.drop_tip()

    protocol.home()
    comment = "End of the first wash"
    comment += "\n* Vacuum pump: 1mn (-200/-400 mbar)"
    protocol.pause(comment)

    protocol.comment("Second wash")
    pipette.pick_up_tip(solvent_tips["A5"])
    for well in vacuum_samples:
        for j in range(4):
            pipette.aspirate(225, a4_wash_buffer1)
            pipette.move_to(a4_wash_buffer1.top().move(
                types.Point(x=0, y=0, z=2)))
            protocol.delay(seconds=1)
            shake(pipette, a4_wash_buffer1, 2)
            pipette.dispense(225, well.top())
    pipette.drop_tip(solvent_tips["A5"])

    protocol.home()
    comment = "\tEnd of the second wash."
    comment += "\n* Vacuum pump: 1mn (-200/-400 mbar)"
    protocol.pause(comment)

    protocol.comment("Third wash")
    pipette.pick_up_tip(solvent_tips["A5"])
    for well in vacuum_samples:
        for j in range(4):
            pipette.aspirate(225, a4_wash_buffer2)
            pipette.move_to(a4_wash_buffer2.top().move(
                types.Point(x=0, y=0, z=2)))
            protocol.delay(seconds=1)
            shake(pipette, a4_wash_buffer2, 2)
            pipette.dispense(225, well.top())
    pipette.drop_tip()

    protocol.home()
    comment = "\tEnd of the third wash."
    comment += "\n* Turn on the vacuum pump: -200/-400 mbar (1min)"
    comment += "\n* Take away the binding plate et the waste"
    comment += "\n* Dry the binding plate to remove any buffer redisue."
    comment += "\n* Put the binding plate on top of the manifold lid"
    comment += "\n* Turn on the vacuum pump: -400/-600 mbar (10-15min)"
    comment += "\n* Put the elution plate into the manifold."
    comment += "\n* Put the binding plate on top of the manifold lid."
    comment += "\n\nIMPORTANT: DON'T START THE PROTOCOL UNTIL THE "
    comment += "VACUUM IS READY FOR THE NEXT STEPS!"
    protocol.pause(comment)

    protocol.comment("Elution step")
    pipette.flow_rate.dispense = 150
    for well in range(len(vacuum_samples)):
        pipette.pick_up_tip(elution_tips.rows()[0][well])
        pipette.aspirate(elution_vol, elution_buffer)
        protocol.delay(seconds=1)
        shake(pipette, elution_buffer, 1)
        pipette.dispense(elution_vol, vacuum_samples[well])
        shake(pipette, vacuum_samples[well], -5)
        pipette.drop_tip()
    protocol.delay(minutes=3)

    comment = "\tEnd of the elution."
    comment += "\n* Turn on the vacuum pump: -400/-600 mbar (1min)"
    comment += "\n\nEND OF THE PROTOCOL."
    protocol.pause(comment)
    protocol.home()
