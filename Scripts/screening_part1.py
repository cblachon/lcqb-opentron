from opentrons import protocol_api
from opentrons import types
import numpy as np
import string
import math

# Test command line:
# python -m opentrons.simulate SCRIPT --custom-labware-path="PATH"

# metadata
metadata = {
    "protocolName": "Screening of complemented PRK mutant - PART 1 "
                    "(dispensing cultures)",
    "author": "Clémence <clemence.blachon@sorbonne-universite.fr>",
    "description": "Protocol to transfer cultures according DO results "
                    "given in csv table",
    "apiLevel": "2.9"}

amplitude = 0.8


def generate_well_list(x, y, col_name, row_name):
    # x étant le nombre de colonnes et y le nombre de lignes
    well_list = list()
    for i in range(x):
        for j in range(y):
            well_list.append(str(row_name[j])+str(col_name[i]))
    return well_list


def shake(pipette, _target, z_offset):
    pipette.move_to(_target.top().move(
        types.Point(x=amplitude, y=0, z=z_offset)))
    pipette.move_to(_target.top().move(
        types.Point(x=-amplitude, y=0, z=z_offset)))
    pipette.move_to(_target.top().move(
        types.Point(x=amplitude, y=0, z=z_offset)))
    pipette.move_to(_target.top().move(
        types.Point(x=-amplitude, y=0, z=z_offset)))


def convert_matrix(data):
    vol_data, useless_wells = dict(), list()
    header = True
    for row in data.splitlines()[1:]:
        split_row = row.split("\t")
        if header is True:
            row_id = split_row
            header = False
        else:
            index = True
            for col in range(len(split_row)):
                if index is True:
                    index_id = split_row[col]
                    index = False
                else:
                    if split_row[col].lower() not in ["none", "null", 0]:
                        x = float(split_row[col])
                        vol_data[index_id+row_id[col]] = x
                    else:
                        useless_wells.append(index_id+row_id[col])
    return vol_data, useless_wells


def transfering_multiple(pipette, pipette_capacity, expected_vol, source_well,
                         dest_well, pick_drop_tip=True):
    if pick_drop_tip is True:
        pipette.pick_up_tip()
    transfered_volume = 0
    for i in range(math.floor(expected_vol/pipette_capacity)):
        pipette.aspirate(pipette_capacity, source_well)
        pipette.dispense(pipette_capacity, dest_well)
        transfered_volume += pipette_capacity
    if transfered_volume < expected_vol:
        pipette.aspirate(expected_vol-transfered_volume, source_well)
        pipette.dispense(expected_vol-transfered_volume, dest_well)
    if pick_drop_tip is True:
        pipette.drop_tip()


def run(protocol: protocol_api.ProtocolContext):
    # Installation
    # Labware
    p20_tip_slots = [10, 11]
    p20_tip_rack = [protocol.load_labware(
        "opentrons_96_filtertiprack_20ul", slot) for slot in p20_tip_slots]
    well_slots = [4, 5, 6, 8]
    well_plates = [protocol.load_labware(
        "vwr_96_wellplate_200ul", slot) for slot in well_slots]
    source_plate = protocol.load_labware("vwr_96_wellplate_200ul", 2)
    reservoir = protocol.load_labware("nest_12_reservoir_15ml", 7)

    # Instruments
    p20 = protocol.load_instrument(
        "p20_single_gen2", "left", tip_racks=p20_tip_rack)
    pipette_capacity = 20

    # Protocol Data
    data = """
    1	2	3	4	5	6	7	8	9	10	11	12
A	60	61	80	91	100	10	20	25	30	150	141	101
B	1	1	1	1	1	1	1	1	1	1	1	1
C	1	1	1	1	1	1	1	1	1	1	1	1
D	1	1	1	1	1	1	1	1	1	1	1	1
E	1	1	1	1	1	1	1	1	1	1	1	1
F	1	1	1	1	1	1	1	1	1	1	1	1
G	1	1	1	1	1	1	1	1	1	1	1	1
H	1	1	1	1	1	1	1	1	1	1	1	1
"""

    concentration_data, useless_wells = convert_matrix(data)
    # WT AND PRK POSITIONS ON SOURCE PLATE
    wt_source = ["A1", "A4", "A7", "A10"]
    prk_source = ["H1", "H4", "H7", "H10"]
    wt_dest, prk_dest = "A1", "H1"
    dest_wells = generate_well_list(3, 8, [1, 5, 9], string.ascii_uppercase)
    source_wells = generate_well_list(
        12, 8, np.arange(1, 13), string.ascii_uppercase)
    for i in useless_wells:
        source_wells.remove(i)

    # Protocol
    protocol.home()
    protocol.comment(
        "PART 1: Dispensing cultures.\nPlease, put the source plate on slot 2 "
        "and the destination plates on the slots 4, 5, 6 and 8.")

    # Dispensing TAP in every plates to have 200uL
    plate_id = 0
    p20.pick_up_tip()
    tap_vol, tap_well = 0, "A1"
    for i in range(0, len(source_wells), len(dest_wells)):
        well_id = 0
        for j in source_wells[i:i+22]:
            transfering_multiple(
                p20, pipette_capacity, 200-concentration_data[j],
                reservoir[tap_well],
                well_plates[plate_id][dest_wells[well_id]],
                pick_drop_tip=False)
            tap_vol += (200-concentration_data[j])
            if tap_vol >= 15000-20:
                tap_well = "A2"
            well_id += 1
        plate_id += 1
    p20.drop_tip()

    # Dispensing WT in every plates with p20 (one at a time)
    for well_id in range(len(well_plates)):
        wt = wt_source[well_id]
        p20.pick_up_tip()
        transfering_multiple(
            p20, pipette_capacity, concentration_data[wt], source_plate[wt],
            well_plates[well_id][wt_dest], pick_drop_tip=False)
        del concentration_data[wt]
        source_wells.remove(wt)
        print(dest_wells, wt_dest, "removing")
        p20.drop_tip()
    dest_wells.remove(wt_dest)

    # Dispensing PRK in every plates
    for well_id in range(len(well_plates)):
        prk = prk_source[well_id]
        p20.pick_up_tip()
        transfering_multiple(
            p20, pipette_capacity, concentration_data[prk], source_plate[prk],
            well_plates[well_id][prk_dest], pick_drop_tip=False)
        source_wells.remove(prk)
        p20.drop_tip()
    dest_wells.remove(prk_dest)

    # Dispensing all the other cultures in every plates
    # (22 transformants for one plate)
    plate_id = 0
    for i in range(0, len(source_wells), len(dest_wells)):
        well_id = 0
        for j in source_wells[i:i+22]:
            p20.pick_up_tip()
            transfering_multiple(
                p20, pipette_capacity, concentration_data[j], source_plate[j],
                well_plates[plate_id][dest_wells[well_id]],
                pick_drop_tip=False)
            shake(p20, well_plates[plate_id][dest_wells[well_id]], -1)
            well_id += 1
            p20.drop_tip()
        plate_id += 1
    protocol.home()
