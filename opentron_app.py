import os
import sys
import shutil
from tkinter import messagebox
from numpy.core.arrayprint import _leading_trailing

import pandas as pd
import numpy as np
import platform
import string
import math
import os 
import re
from tkinter import *
from tkinter.messagebox import *
from tkinter.filedialog import *
from tkinter.simpledialog import *

#ENVIRONMENT
main_path = os.getcwd()
os_platform = platform.system()
job_name = 'opentron_app'
plate_dict = {'ampbio_96_wellplate_200ul':(12,8), 'opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap':(6,4), 'corning_384_wellplate_180ul':(24,16), 'greiner_96_wellplate_20ul':(12,8), 'blocfroid_24_wellplate_1500ul':(6,4), 'corning_384_wellplate_112ul_flat':(24,16), 'nest_96_wellplate_200ul_flat':(12,8)}


def create_plate_index(x, y, vertical = True, horizontal = False):
	if vertical == True:
		index = (list(string.ascii_uppercase)[0:y], np.arange(1, x+1))
		plate_index = list()
		for i in index[1]:
			for j in index[0]:
				plate_index.append(j+str(i))
	elif horizontal == True:
		index = (list(string.ascii_uppercase)[0:y], np.arange(1, x+1))
		plate_index = list()
		for i in index[0]:
			for j in index[1]:
				plate_index.append(i+str(j))		
	return plate_index

class WrongSize(Exception):
	pass

class app_interface:
	def __init__(self):
		#Paramètres de ma fenêtre
		self.window = Tk()
		self.window.title('Biodata')
		self.window.geometry('500x300')
		self.window.config(background = 'white')
		self.window.resizable(False, False)
		self.main_frame = Frame(self.window, bg = 'white')
		self.robot_frame = LabelFrame(self.main_frame, text = 'OT2 organisation', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
		pipette_list = ('p20_single_gen2', 'p300_multi', 'None')
		self.left_pipette_var =  StringVar(self.window)
		self.left_pipette_var.set(pipette_list[0])
		Label(self.robot_frame, text = "Left pipette:", bg='white').grid(column=0, row=0, sticky='nesw')
		left_pipette = OptionMenu(self.robot_frame, self.left_pipette_var, *pipette_list)
		left_pipette.config(bg='white')
		left_pipette.grid(column=1, row=0, sticky='nesw')
		self.right_pipette_var =  StringVar(self.window)
		self.right_pipette_var.set(pipette_list[1])
		Label(self.robot_frame, text = "Right pipette:", bg='white').grid(column=0, row=1, sticky='nesw')
		right_pipette = OptionMenu(self.robot_frame, self.right_pipette_var, *pipette_list)
		right_pipette.config(bg='white')
		right_pipette.grid(column=1, row=1, sticky='nesw')
		self.robot_frame.grid(column=0,  row=0, columnspan=2, padx = 10, sticky='nesw')

		self.moclo_frame = LabelFrame(self.main_frame, text = 'MoClo assembly', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
		Label(self.moclo_frame, text='MoClo assembly OT2 scripts generator', bg='white').grid(row=0, column=0)
		Button(self.moclo_frame, text='MoClo', command = self.moclo_window, bg='white').grid(row=1, column=0)
		self.moclo_frame.grid(column = 0, row = 1, padx = 10, sticky = 'nesw')

		self.transfo_frame = LabelFrame(self.main_frame, text = 'Transformations', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
		Label(self.transfo_frame, text='Transformations OT2 scripts generator', bg='white').grid(row=0, column=0)
		Button(self.transfo_frame, text='Transformations', command=self.transfo_window, bg='white').grid(row=1, column=0)
		self.transfo_frame.grid(column = 1, row = 1, padx = 10, sticky = 'nesw')

		self.spotting_frame = LabelFrame(self.main_frame, text = 'Spotting', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
		Label(self.spotting_frame, text='Spot test OT2 scripts generator', bg='white').grid(row=0, column=0)
		Button(self.spotting_frame, text='Spotting', command=self.spotting_window, bg='white').grid(row=1, column=0)
		self.spotting_frame.grid(column = 0, row = 2, padx = 10, sticky = 'nesw')

		self.aliquot_frame = LabelFrame(self.main_frame, text = 'Aliquots', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
		Label(self.aliquot_frame, text='Aliquots OT2 scripts generator', bg='white').grid(row=0, column=0)
		Button(self.aliquot_frame, text='Aliquot', command=self.aliquots_window, bg='white').grid(row=1, column=0)
		self.aliquot_frame.grid(column = 1, row = 2, padx = 10, sticky = 'nesw')

		self.main_frame.pack(fill = BOTH)

		self.menu_bar = Menu(self.window)
		self.create_menu()
		self.window.config(menu = self.menu_bar)

		self.window.mainloop()

	def create_menu(self):
		'''Fonction pour créer le menu avec différentes commandes disponibles'''
		self.file_menu = Menu(self.menu_bar, tearoff = 0) 
		#self.file_menu.add_command(label='Sauvegarder', command = self.save_canvas)
		#self.file_menu.add_command(label='Read_me', command = self.open_readme)
		self.file_menu.add_command(label='Aide', command = self.user_help)
		self.menu_bar.add_cascade(label='Help', menu = self.file_menu)

	def user_help(self):
		'''Fonction qui permet d'affichier une fenetre d'aide si l'utilisateur en a besoin'''
		help_window = Toplevel()
		help_window.wm_geometry('700x500')
		text_frame = LabelFrame(help_window, text = 'Aide', font = ('Verdana', '10', 'bold', 'underline'), fg = '#0da7ed', bg='white')
		help_file = open('help.txt', 'r')
		help_text = ''.join(help_file.readlines())
		help_file.close()
		text = Label(text_frame, text = help_text, font = ('Verdana', '10'), justify = 'left', bg='white', wraplength=600)
		text.pack(padx=10, pady=10)
		text_frame.pack(expand = YES, fill = 'both')

	def moclo_window(self):
		try:
			left_pipette = self.left_pipette_var.get()
			right_pipette = self.right_pipette_var.get()
			if (left_pipette != 'p20_single_gen2' and right_pipette != 'p20_single_gen2'):
				raise ValueError
			data_window = Toplevel(bg='white')
			data_window.title('MoClo')
			data_window.wm_geometry('450x350')
			self.menu_bar = Menu(data_window)
			self.create_menu()
			data_window.config(menu = self.menu_bar)
			self.moclo_jobname_frame = LabelFrame(data_window, text = 'Job name', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			self.moclo_jobname_var = StringVar()
			Label(self.moclo_jobname_frame, text='Job name:', bg='white').grid(row=0, column=0, sticky='nesw')
			self.moclo_jobname = Entry(self.moclo_jobname_frame, text='job_name', textvariable=self.moclo_jobname_var, borderwidth=2).grid(row=0, column=1, padx=5, pady=5, sticky='nesw')
			self.moclo_jobname_frame.grid(row = 0, column=0, columnspan=2, padx=10, pady=10, sticky='nesw')
			self.moclo = LabelFrame(data_window, text = 'MoClo', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			plate_list = list(plate_dict.keys())
			self.moclo_plate_var =  StringVar(data_window)
			self.moclo_plate_var.set(plate_list[0])
			moclo_plate = OptionMenu(self.moclo, self.moclo_plate_var, *plate_list)
			Label(self.moclo, text = "Source plate:", bg='white').grid(column=0, row=0, sticky='nesw')
			moclo_plate.config(bg='white')
			moclo_plate.grid(column=1, row=0, sticky='nesw')
			temp_cycle = ('120 min at 42°C and 20 min at 80°C', 'Outer thermocycler')
			self.moclo_temp_cycle =  StringVar(data_window)
			self.moclo_temp_cycle.set(temp_cycle[0])
			moclo_temp = OptionMenu(self.moclo, self.moclo_temp_cycle, *temp_cycle)
			Label(self.moclo, text = "Temperature cycles:", bg='white').grid(column=0, row=1, sticky='nesw')
			moclo_temp.config(bg='white')
			moclo_temp.grid(column=1, row=1, sticky='nesw')
			self.moclo.grid(column = 0, row = 1, columnspan=2, padx = 10, pady=10, sticky = 'nesw')
			self.script_button = Button(data_window, text='Download template', command = self.dl_moclo_template, bg='white').grid(column=0, row=2, padx=10, sticky='nesw')
			self.script_button = Button(data_window, text='Input data', command = self.create_moclo_script, bg='white').grid(column=1, row=2, padx=10, sticky='nesw')
			Button(data_window, text='Quitter', command=data_window.quit).grid(row=4, column=0, padx=10, pady=10, sticky='sw')
			data_window.mainloop()
		except:
			messagebox.showerror(title='Pipette selection', message='You must have at least one p20 pipette (either on left or right).')

	def dl_moclo_template(self):
		try:
			if self.moclo_jobname_var.get() == '' or self.moclo_jobname_var.get() == 'opentron_app':
				raise ValueError
			template_name = self.moclo_jobname_var.get().replace(' ', '_')+'_DATA.xlsx'
			shutil.copyfile('data/Ligation_moclo.xlsx', template_name)
			messagebox.showinfo(title='Resulting script', message='Your template is available here:\n%s\%s.py' %(main_path, template_name))
		except:
			messagebox.showerror(title='Job name', message='Please enter a job name')
		return None

	def read_moclo_xlsx(self):
		assembly_dict = dict()
		source_file = askopenfilename(filetypes=[("Excel files", "*.xlsx")])
		xls = pd.ExcelFile(source_file)
		wells = ['A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'A12', 'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12', 'E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7', 'E8', 'E9', 'E10', 'E11', 'E12', 'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12', 'G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9', 'G10', 'G11', 'G12', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'H9', 'H10', 'H11', 'H12']
		index = 0
		for sheet in xls.sheet_names:
			if sheet not in ['HOW TO', 'Plate', 'Data MoClo', 'Data Chlamy MoClo', 'Data Level 1 Chlamy']:
				print('\n', sheet)
				dest = wells[index]
				index += 1
				sheet_data = pd.read_excel(source_file, sheet_name=sheet, header=None)
				vol_water = sheet_data.iloc[37, 1]
				print(vol_water)
				print(sheet_data.iloc[43, 1], sheet_data.iloc[44, 1], sheet_data.iloc[45, 1], sheet_data.iloc[46, 1])
				vol_master_mix = float(sheet_data.iloc[43, 1])+float(sheet_data.iloc[44, 1])+float(sheet_data.iloc[45, 1])+float(sheet_data.iloc[46, 1])
				print(sheet_data.iloc[1, 0])
				if sheet_data.iloc[1, 0] == 'Level 0':
					vol_partA = sheet_data.iloc[36, 1]
					well_partA = sheet_data.iloc[9, 4]
					vol_partB = sheet_data.iloc[36, 2]
					well_partB = sheet_data.iloc[10, 4]
					vol_backbone = sheet_data.iloc[36, 5]
					well_backbone = sheet_data.iloc[12, 6]
					assembly_dict[dest] = (well_backbone, vol_backbone, well_partA, vol_partA, well_partB, vol_partB, vol_master_mix+vol_water)
				elif sheet_data.iloc[1, 0] == 'Level 1':
					assembly_dict[dest] = list()
					vol_backbone = sheet_data.iloc[36, 10]
					assembly_dict[dest].append(sheet_data.iloc[14, 8])
					assembly_dict[dest].append(vol_backbone)
					part_index = 0
					for i in range(7):
						vol_part = sheet_data.iloc[36, i+1]
						assembly_dict[dest].append(sheet_data.iloc[6+part_index, 8])
						assembly_dict[dest].append(vol_part)
						part_index += 1
					assembly_dict[dest].append(vol_master_mix+vol_water)
				elif sheet_data.iloc[1, 0] == 'Level M':
					assembly_dict[dest] = list()
					vol_backbone = sheet_data.iloc[36, 10]
					assembly_dict[dest].append(sheet_data.iloc[14, 9])
					assembly_dict[dest].append(vol_backbone)
					vol_linker = sheet_data.iloc[36, 8]
					assembly_dict[dest].append(sheet_data.iloc[15, 9])
					assembly_dict[dest].append(vol_linker)
					part_index = 0
					for i in range(6):
						vol_part = sheet_data.iloc[36, i+1]
						assembly_dict[dest].append(sheet_data.iloc[6+part_index, 9])
						assembly_dict[dest].append(vol_part)
						part_index += 1
					assembly_dict[dest].append(vol_master_mix+vol_water)
		print(assembly_dict)
		return assembly_dict

	def create_moclo_script(self):
		try:
			if self.moclo_jobname_var.get() == '' or self.moclo_jobname_var.get() == 'opentron_app':
				raise ValueError
			assembly_dict = self.read_moclo_xlsx()
			self.write_moclo_script(assembly_dict)
		except:
			messagebox.showerror(title='Job name', message='Please enter a job name')
	
	def write_moclo_script(self, assembly_dict):
		script_file = open('./Scripts/moclo.py', 'r')
		script = script_file.readlines()
		script_file.close()
		script_name = self.moclo_jobname_var.get().replace(' ', '_')
		script_file = open('%s.py' % (script_name), 'w')
		script[14] = '\tassembly_dict = %s\n' % (str(assembly_dict))
		script[20] = "\tbank = protocol.load_labware('%s', 1)\n" % (self.moclo_plate_var.get())
		if self.left_pipette_var == 'p20_single_gen2':
			script[27] = "\tp20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks=tip_rack)\n"
		elif self.right_pipette_var == 'p20_single_gen2':
			script[27] = "\tp20 = protocol.load_instrument('p20_single_gen2', 'right', tip_racks=tip_rack)\n"
		if self.moclo_temp_cycle.get() == 'Outer thermocycler':
			script_file.writelines(script[:-5])
		else:
			script_file.writelines(script)
		script_file.close()
		messagebox.showinfo(title='Resulting script', message='Your script is available here:\n%s\%s.py' %(main_path, script_name))
		return None

	def transfo_window(self):
		try:
			left_pipette = self.left_pipette_var.get()
			right_pipette = self.right_pipette_var.get()
			if (left_pipette == 'p20_single_gen2' and right_pipette == 'p20_single_gen2') or (left_pipette == 'p300_multi' and right_pipette == 'p300_multi') or left_pipette=='None' or right_pipette=='None':
				print('here', right_pipette, left_pipette)
				raise ValueError
			self.transfo_data_window = Toplevel(bg='white')
			self.transfo_data_window.title('Transformation')
			self.transfo_data_window.wm_geometry('1300x550')
			self.menu_bar = Menu(self.transfo_data_window)
			self.create_menu()
			self.transfo_data_window.config(menu = self.menu_bar)
			self.job_name_frame = LabelFrame(self.transfo_data_window, text = 'Job name', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			Label(self.job_name_frame, text='Job name:', bg='white').grid(row=0, column=0, sticky='nesw')
			self.jobname_var = StringVar()
			self.jobname = Entry(self.job_name_frame, text='job_name', textvariable=self.jobname_var, borderwidth=2).grid(row=0, column=1, padx=5, pady=5, sticky='nesw')
			self.job_name_frame.grid(row = 0, column=0, padx=10, pady=10, sticky='nesw')
			self.transfo = LabelFrame(self.transfo_data_window, text = 'Transformations', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			nb_transfo_var = DoubleVar()
			nb_transfo_var.set(8)
			self.nb_transfo = Scale(self.transfo, orient='horizontal', from_=1, to=24, variable=nb_transfo_var, label='Number of tranformations', bg='white', command=self.change_nbdil_limits)
			self.nb_transfo.grid(column=0, row=0, columnspan=2, sticky=E+W)
			lb_vol_var = DoubleVar()
			lb_vol_var.set(150)
			self.lb_vol = Scale(self.transfo, orient='horizontal', from_=1, to=150, variable=lb_vol_var, label='LB (µL)', bg='white')
			self.lb_vol.grid(column=0, row=1, columnspan=2, sticky=E+W)
			Label(self.transfo, text = "Source plate:", bg='white').grid(column=0, row=2, sticky='nesw')
			plate_list = list(plate_dict.keys())
			self.plate_var =  StringVar(self.transfo_data_window)
			self.plate_var.set(plate_list[0])
			entry_plate = OptionMenu(self.transfo, self.plate_var, *plate_list, command=self.change_plate_frame)
			entry_plate.config(bg='white')
			entry_plate.grid(column=1, row=2, sticky='nesw')
			self.transfo.grid(column = 0, row = 1, padx = 10, pady=10, sticky = 'nesw')
			self.transfo_spotting = LabelFrame(self.transfo_data_window, text = 'Spotting', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			self.yes_no_dilution = StringVar()
			self.yes_no_dilution.set('yes')
			dilution_label = Label(self.transfo_spotting, text = "Spotting protocol after transformation?", bg='white').grid(column=0, row=0, columnspan=2, sticky='nesw')
			self.yes_dilution = Radiobutton(self.transfo_spotting, text='Yes', var=self.yes_no_dilution, value='yes', command=self.activate_dilution, bg='white').grid(column=0, row=1, sticky='nesw')
			self.no_dilution  = Radiobutton(self.transfo_spotting, text='No', var=self.yes_no_dilution, value='no', command=self.deactivate_dilution, bg='white').grid(column=1, row=1, sticky='nesw')
			transfo_nb_dilution_var = DoubleVar()
			self.transfo_nb_dilution = Scale(self.transfo_spotting, orient='horizontal', from_=1, to=6, variable=transfo_nb_dilution_var, label='Number of dilutions', bg='white')
			self.transfo_nb_dilution.grid(column=0, row=2, columnspan=2, sticky=E+W)
			transfo_dilution_factor_var = DoubleVar()
			self.transfo_dilution_factor = Scale(self.transfo_spotting, orient='horizontal', from_=2, to=10, variable=transfo_dilution_factor_var, label='Dilution factor', bg='white')
			self.transfo_dilution_factor.grid(column=0, row=3, columnspan=2, sticky='ew')
			self.transfo_spotting.grid(column = 0, row = 2, padx = 10, pady=10, sticky = 'nesw')
			self.plate_frame = LabelFrame(self.transfo_data_window, text = 'Plate data', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', width=900, borderwidth = 2)
			self.plate_frame.grid_propagate(0)
			width = plate_dict[self.plate_var.get()][0]
			height = plate_dict[self.plate_var.get()][1]
			self.draw_interactive_table(height, width)
			self.script_button = Button(self.transfo_data_window, text='Generate script', command = self.create_transfo_script, bg='white').grid(column=0, row=3, padx=10, sticky='nesw')
			self.download_button = Button(self.transfo_data_window, text='Download template', command = self.download_transfo_script, bg='white').grid(column=1, row=3, padx=10, sticky='nesw')
			self.upload_button = Button(self.transfo_data_window, text='Upload template', command = self.upload_transfo_script, bg='white').grid(column=2, row=3, padx=10, sticky='nesw')
			self.plate_frame.grid(column = 1, columnspan=2, row = 0, rowspan=3, padx = 10, pady=10, sticky = 'nesw')
			Button(self.transfo_data_window, text='Quitter', command=self.transfo_data_window.quit).grid(row=4, column=0, padx=10, pady=10, sticky='sw')
			self.transfo_data_window.mainloop()
		except:
			messagebox.showerror(title='Pipette selection', message='You must have a p20 and p300 pipettes to use this protocole.')

	def upload_transfo_script(self):
		template_file = askopenfilename()
		template_data = open(template_file, 'r')
		table_row = 0
		for line in template_data:
			if line[0] == '#':
				s_line = line[1:].strip().split(': ')
				if s_line[0] == 'Jobname':
					self.jobname.insert(0, s_line[1])
				elif s_line[0] == 'Nb transfo':
					self.nb_transfo.set(int(s_line[1]))
				elif s_line[0] == 'LB vol':
					self.lb_vol.set(int(s_line[1]))
				elif s_line[0] == 'Source plate':
					self.plate_var.set(s_line[1])
					self.change_plate_frame(s_line[1])
				elif s_line[0] == 'Spotting':
					spotting = s_line[1]
					self.yes_no_dilution.set(spotting)
				elif s_line[0] == 'Nb of dilutions' and spotting == 'yes':
					self.transfo_nb_dilution.set(int(s_line[1]))
				elif s_line[0] == 'Dilution factor' and spotting == 'yes':
					self.transfo_dilution_factor.set(int(s_line[1]))
			elif line[0].isalpha():
				s_line = line[1:].strip().split()
				table_col = 0
				for j in s_line:
					if j != '0':
						self.data[table_row][table_col].insert(0, j)
					table_col += 1
				table_row += 1
		template_data.close()

	def download_transfo_script(self):
		try:
			if self.jobname_var.get() == '' or self.jobname_var.get() == 'opentron_app':
				raise ValueError
			template_name = self.jobname_var.get().replace(' ', '_')
			template_file = open('%s.csv' % (template_name), 'w')
			template_file.write('#Job name: %s\n' % (self.jobname_var.get()))
			template_file.write('#Nb transfo: %i\n' % (self.nb_transfo.get()))
			template_file.write('#LB vol: %i\n' % (self.lb_vol.get()))
			template_file.write('#Source plate: %s\n' % (self.plate_var.get()))
			template_file.write('#Spotting: %s\n' % (self.yes_no_dilution.get()))
			template_file.write('#Nb of dilutions: %s\n' % (self.transfo_nb_dilution.get()))
			template_file.write('#Dilution factor: %s\n' % (self.transfo_dilution_factor.get()))
			rowname = ['A', 'B', 'C', 'D', 'E', 'F', 'G' ,'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']
			for i in range(self.numberColumns):
				template_file.write('\t%i' % (i+1))
			for i in range(self.numberLines):
				line = '\n%s' % (rowname[i])
				for j in range(self.numberColumns):
					if self.data[i][j].get() != '':
						line += '\t%s' % (self.data[i][j].get())
					else:
						line += '\t%i' % (0)
				template_file.write(line)
			template_file.close()
		except ValueError:
			messagebox.showerror(title='Job name', message='Please enter a job name')

	def change_plate_frame(self, val):
		for widgets in self.plate_frame.winfo_children():
			widgets.destroy()
		width = plate_dict[self.plate_var.get()][0]
		height = plate_dict[self.plate_var.get()][1]
		self.draw_interactive_table(height, width)

	def check_entry(self, input):
		try:
			if input == '':
				return True
			elif float(input) >= 1 and float(input) <= 20:
				return True
			else:
				messagebox.showerror(title='Value error', message='You must enter a volume (int or float) between 1 and 20µL)')
				return False
		except ValueError:
			messagebox.showerror(title='Value error', message='You must enter a volume (int or float) between 1 and 20µL)')
			return False

	def draw_interactive_table(self, height, width):
		self.numberLines = height
		self.numberColumns = width
		self.data = list()
		reg = self.transfo_data_window.register(self.check_entry)
		rowname = ['A', 'B', 'C', 'D', 'E', 'F', 'G' ,'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']
		for i in range(1, self.numberColumns+1):
			cell = Label(self.plate_frame, text=i).grid(row=0, column=i)
		for i in range(self.numberLines):			
			line = list()
			Label(self.plate_frame, text=rowname[i]).grid(row=i+1, column=0)
			for j in range(self.numberColumns):
				cell = Entry(self.plate_frame, width=5, validate='key', validatecommand =(reg, '%P'))
				line.append(cell)
				cell.grid(row = i+1, column = j+1)
			self.data.append(line)

	def change_nbdil_limits(self, val):
		if int(val) > 16:
			self.transfo_nb_dilution.config(to=4)
		else:
			self.transfo_nb_dilution.config(to=6)

	def activate_dilution(self):
		self.transfo_dilution_factor.config(state='active')
		self.transfo_nb_dilution.config(state='active')
	
	def deactivate_dilution(self):
		self.transfo_dilution_factor.config(state='disabled')
		self.transfo_nb_dilution.config(state='disabled')

	def create_plate_df(self):
		rowname = ['A', 'B', 'C', 'D', 'E', 'F', 'G' ,'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P']
		ar = np.zeros((self.numberLines, self.numberColumns))
		for i in range(self.numberLines): 
			for j in range(self.numberColumns):
				if self.data[i][j].get() != '':
					ar[i][j] = int(self.data[i][j].get())
		df = pd.DataFrame(ar, index = rowname[0:self.numberLines], columns = range(1, self.numberColumns+1))
		return df

	def create_transfo_script(self):
		try:
			if self.jobname_var.get() == '' or self.jobname_var.get() == 'opentron_app':
				raise ValueError
			cols = ['1', '2', '3', '4', '5', '6', '7', '8', '9','10','11', '12']
			if self.nb_transfo.get() <= 8:
				destination_plate = ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1']
				used_col = ', '.join(cols[0:self.transfo_nb_dilution.get()])
			elif self.nb_transfo.get() <= 16:
				destination_plate = ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'A7', 'B7', 'C7', 'D7', 'E7', 'F7', 'G7', 'H7']
				used_col = ', '.join(cols[0:self.transfo_nb_dilution.get()]+cols[6:6+self.transfo_nb_dilution.get()])
			else:
				destination_plate = ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'A5', 'B5', 'C5', 'D5', 'E5', 'F5', 'G5', 'H5', 'A9', 'B9', 'C9', 'D9', 'E9', 'F9', 'G9', 'H9']
				used_col = ', '.join(cols[0:self.transfo_nb_dilution.get()]+cols[4:4+self.transfo_nb_dilution.get()]+cols[8:8+self.transfo_nb_dilution.get()])
			dest_index = 0
			transfer_dict = dict()	
			transfer_details = '\tResume:\n'
			data = self.create_plate_df()
			nb_rows = len(data.index) 
			nb_col = len(data.columns)
			for col in range(nb_col):
				for row in range(nb_rows):
					if data.iat[row, col] > 0:
						transfer_dict[data.index[row]+str(data.columns[col])] = (destination_plate[dest_index], data.iat[row, col])
						transfer_details += '*%iuL from %s to %s\n' % (data.iat[row, col], data.index[row]+str(data.columns[col]), destination_plate[dest_index])
						dest_index += 1
			transfer_details += '\n\nYou must also prepare a 12 reservoirs with LB in the first column and water in the second column.'
			transfer_details += '\n\n200µL strips must be put on those columns: %s' % (used_col)
			if len(transfer_dict) > self.nb_transfo.get():
				messagebox.showerror(title='Error', message='There are too many sources of DNA compared to the number of transformations.')
			elif len(transfer_dict) < self.nb_transfo.get():
				messagebox.showerror(title='Error', message='There is not enough sources of DNA compared to the number of transformations.')
			else:
				all_col = ['A1', 'A5', 'A9']
				if math.ceil(self.nb_transfo.get()/8) == 1:
					dest_col = ['A1']
				elif math.ceil(self.nb_transfo.get()/8) == 2:
					dest_col = ['A1', 'A7']
				elif math.ceil(self.nb_transfo.get()/8) == 3:
					dest_col = ['A1', 'A5', 'A9']
				self.write_transfo_script(transfer_dict, dest_col, transfer_details)
		except:
			messagebox.showerror(title='Job name', message='Please enter a job name')

	def write_transfo_script(self, transfo_dict, dest_col, transfer_details):
		script_file = open('./Scripts/transformations.py', 'r')
		script = script_file.readlines()
		script_file.close()
		script_name = self.jobname_var.get().replace(' ', '_')
		script_file = open('%s.py' % (script_name), 'w')
		if self.left_pipette_var.get() == 'p300_multi':
			script[23] = "\tp300 = protocol.load_instrument('p300_multi', 'left', tip_racks=[tiprack_p300])\n"
		elif self.right_pipette_var.get() == 'p300_multi':
			script[23] = "\tp300 = protocol.load_instrument('p300_multi', 'right', tip_racks=[tiprack_p300])\n"
		if self.left_pipette_var.get() == 'p20_single_gen2':
			script[22] = "\tp20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks=[tiprack_p20])\n"
		elif self.left_pipette_var.get() == 'p20_single_gen2':
			script[22] = "\tp20 = protocol.load_instrument('p20_single_gen2', 'right', tip_racks=[tiprack_p20])\n"
		script[16] = "\ttube_rack = protocol.load_labware('%s', 6)\n" % (self.plate_var.get())
		script[26] = '\tinput_dict, dest_col = (%s, %s)\n' % (str(transfo_dict), str(dest_col))
		script[27] = '\tnb_transfo = %i\n' % (self.nb_transfo.get())
		script[28] = '\tlb_vol = %i\n' % (self.lb_vol.get())
		if self.yes_no_dilution.get() == 'yes':
			script[68] = '\tnb_dilution = %i\n' % (self.transfo_nb_dilution.get())
			script[69] = '\tdilution_factor = %i\n' % (self.transfo_dilution_factor.get())
			if math.ceil(self.nb_transfo.get()/8) <= 2:
				script[72] = "\tcolumn = [['A1', 'A2', 'A3', 'A4', 'A5', 'A6'], ['A7', 'A8', 'A9', 'A10', 'A11', 'A12']]\n"
			else:
				script[72] = "\tcolumn = [['A1', 'A2', 'A3', 'A4'], ['A5', 'A6', 'A7', 'A8'], ['A9', 'A10', 'A11', 'A12']]\n"
			script_file.writelines(script)
		elif self.yes_no_dilution.get() == 'no':
			script_file.writelines(script[:-28])
		script_file.close()
		messagebox.showinfo(title='Resulting script', message='%s\n\nYour script is available here:\n%s\%s.py' %(transfer_details, main_path, script_name))
		return None

	def spotting_window(self):
		try:
			left_pipette = self.left_pipette_var.get()
			right_pipette = self.right_pipette_var.get()
			if (left_pipette != 'p300_multi' and right_pipette != 'p300_multi'):
				raise ValueError
			self.spotting_data_window = Toplevel(bg='white')
			self.spotting_data_window.title('Spotting')
			self.spotting_data_window.wm_geometry('350x500')
			self.menu_bar = Menu(self.spotting_data_window)
			self.create_menu()
			self.spotting_data_window.config(menu = self.menu_bar)
			self.spotting_job_name_frame = LabelFrame(self.spotting_data_window, text = 'Job name', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			Label(self.spotting_job_name_frame, text='Job name:', bg='white').grid(row=0, column=0, sticky='nesw')
			self.spot_jobname_var = StringVar()
			self.spot_jobname = Entry(self.spotting_job_name_frame, text='job_name', textvariable=self.spot_jobname_var, borderwidth=2).grid(row=0, column=1, padx=5, pady=5, sticky='nesw')
			self.spotting_job_name_frame.grid(row = 0, column=0, padx=10, pady=10, sticky='nesw')
			self.spotting = LabelFrame(self.spotting_data_window, text = 'Spotting', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			nb_plate_var = DoubleVar()
			Label(self.spotting, text='Source plate:', bg='white').grid(row=0, column=0, sticky='nesw')
			Label(self.spotting, text='1 x 96 well plate', bg='white').grid(row=0, column=1, sticky='nesw')
			self.nb_dest_plate = StringVar()
			self.nb_dest_plate.set('1')
			Label(self.spotting, text='Number of destination plates:', bg='white').grid(row=1, column=0, sticky='nesw')
			Label(self.spotting, textvariable=self.nb_dest_plate, bg='white').grid(row=1, column=1, sticky='nesw')
			nb_col_var = DoubleVar()
			Label(self.spotting, text='NB: The destination plates number represents the number of 96 well-plates and the number of petri dish you need to perform dilutions and spotting.', bg='white', wraplength=250, fg = '#0da7ed').grid(columnspan=2, column=0, row=2, sticky='nesw')
			self.nb_plate_spot = Scale(self.spotting, orient='horizontal', from_=1, to=9, variable=nb_col_var, label='Number of columns to dilute', bg = 'white', command=self.change_nbdil_spot_limits)
			self.nb_plate_spot.grid(column=0, row=3, columnspan=2, sticky='nesw')
			nb_dilution_var = DoubleVar()
			self.nb_dilution = Scale(self.spotting, orient='horizontal', from_=1, to=6, variable=nb_dilution_var, label='Number of dilutions', bg='white')
			self.nb_dilution.grid(column=0, row=4, columnspan=2, sticky='nesw')
			dilution_factor_var = DoubleVar()
			self.dilution_factor = Scale(self.spotting, orient='horizontal', from_=2, to=10, variable=dilution_factor_var, label='Dilution factor', bg='white')
			self.dilution_factor.grid(column=0, row=5, columnspan=2, sticky='nesw')
			self.spotting.grid(column = 0, row = 1, padx = 10, pady=10, sticky = 'nesw')
			self.script_button = Button(self.spotting_data_window, text='Generate script', command = self.create_spotting_script, bg='white').grid(column=0, row=3, padx=10, sticky='nesw')
		except:
			messagebox.showerror(title='Pipette selection', message='You must have a p300 pipette to use this protocole.')

	def change_nbdil_spot_limits(self, val):
		print(val, self.nb_dilution.get(), int(val) * self.nb_dilution.get())
		if int(val) >= 7:
			self.nb_dilution.config(to=4)
		else:
			self.nb_dilution.config(to=6)
		if 0 < int(val) * self.nb_dilution.get() <= 12:
			self.nb_dest_plate.set('1')
		elif 12 < int(val) * self.nb_dilution.get() <= 24:
			self.nb_dest_plate.set('2')
		elif 24 < int(val) * self.nb_dilution.get() <= 36:
			self.nb_dest_plate.set('3')

	def create_spotting_script(self):
		try:
			if self.spot_jobname_var.get() == '' or self.spot_jobname_var.get() == 'opentron_app':
				raise ValueError
			print(os.getcwd())
			self.write_spotting_script()
		except:
			messagebox.showerror(title='Job name', message='Please enter a job name')

	def write_spotting_script(self):
		script_file = open('./Scripts/spotting.py', 'r')
		script = script_file.readlines()
		script_file.close()
		script_name = self.spot_jobname_var.get().replace(' ', '_')
		script_file = open('%s.py' % (script_name), 'w')
		if self.left_pipette_var.get() == 'p300_multi':
			script[27] = "\tp300 = protocol.load_instrument('p300_multi', 'left', tip_racks=tiprack_p300)\n"
		elif self.right_pipette_var.get() == 'p300_multi':
			script[27] = "\tp300 = protocol.load_instrument('p300_multi', 'right', tip_racks=[tiprack_p300])\n"
		script[30] = '\tnb_col = %i\n' % (self.nb_plate_spot.get())
		script[31] = '\tnb_dilution = %i\n' % (self.nb_dilution.get())
		script[32] = '\tdilution_factor = %i\n' % (self.dilution_factor.get())
		script[19] = '\tnb_plates = %s\n' % (self.nb_dest_plate.get())
		script_file.writelines(script)
		script_file.close()
		messagebox.showinfo(title='Resulting script', message='You must fill the following columns: %s\n\nYour script is available here:\n%s\%s.py' %(list(np.arange(1, self.nb_plate_spot.get()+1)), main_path, script_name))
		return None

	def aliquots_window(self):
		try:
			left_pipette = self.left_pipette_var.get()
			right_pipette = self.right_pipette_var.get()
			if (left_pipette != 'p20_single_gen2' and right_pipette != 'p20_single_gen2'):
				raise ValueError
			self.aliquots_data_window = Toplevel(bg='white')
			self.aliquots_data_window.title('aliquots')
			self.aliquots_data_window.wm_geometry('350x500')
			self.menu_bar = Menu(self.aliquots_data_window)
			self.create_menu()
			self.aliquots_data_window.config(menu = self.menu_bar)
			self.aliquots_job_name_frame = LabelFrame(self.aliquots_data_window, text = 'Job name', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			Label(self.aliquots_job_name_frame, text='Job name:', bg='white').grid(row=0, column=0, sticky='nesw')
			self.spot_jobname_var = StringVar()
			self.spot_jobname = Entry(self.aliquots_job_name_frame, text='job_name', textvariable=self.spot_jobname_var, borderwidth=2).grid(row=0, column=1, padx=5, pady=5, sticky='nesw')
			self.aliquots_job_name_frame.grid(row = 0, column=0, padx=10, pady=10, sticky='nesw')
			self.aliquots = LabelFrame(self.aliquots_data_window, text = 'aliquots', font = ('Helvetica', '15', 'bold'), fg = '#0da7ed', bg = 'white', borderwidth = 2)
			Label(self.aliquots, text='Source plate:', bg='white').grid(row=0, column=0, sticky='nesw')
			plate_list = list(plate_dict.keys())
			self.source_plate_var =  StringVar(self.aliquots_data_window)
			self.source_plate_var.set(plate_list[0])
			entry_plate = OptionMenu(self.aliquots, self.source_plate_var, *plate_list, command=self.change_nb_aliquots_limit)
			entry_plate.config(bg='white')
			entry_plate.grid(column=1, row=0, sticky='nesw')
			Label(self.aliquots, text='Destination plate:', bg='white').grid(row=1, column=0, sticky='nesw')
			self.dest_plate_var =  StringVar(self.aliquots_data_window)
			self.dest_plate_var.set(plate_list[0])
			entry_plate = OptionMenu(self.aliquots, self.dest_plate_var, *plate_list, command=self.change_nb_aliquots_limit)
			entry_plate.config(bg='white')
			entry_plate.grid(column=1, row=1, sticky='nesw')
			nb_aliquots_var = DoubleVar()
			self.nb_aliquots = Scale(self.aliquots, orient='horizontal', from_=1, to=24, variable=nb_aliquots_var, label='Number of aliquots', bg = 'white', command=self.change_nb_aliquots_limit)
			self.nb_aliquots.grid(column=0, row=2, columnspan=2, sticky='nesw')
			volume_var = DoubleVar()
			self.volume = Scale(self.aliquots, orient='horizontal', from_=1, to=20, variable=volume_var, label='Volume to transfer', bg='white')
			self.volume.grid(column=0, row=3, columnspan=2, sticky='nesw')
			Label(self.aliquots, text='Source well:', bg='white').grid(row=4, column=0, sticky='nesw')
			self.source_well_var = StringVar()
			self.source_well = Entry(self.aliquots, text='volume aliquots', textvariable=self.source_well_var, borderwidth=2)
			self.source_well.grid(column=1, row=4, sticky='nesw')
			self.aliquots.grid(column = 0, row = 1, padx = 10, pady=10, sticky = 'nesw')
			self.script_button = Button(self.aliquots_data_window, text='Generate script', command = self.create_aliquots_script, bg='white').grid(column=0, row=3, padx=10, sticky='nesw')
		except:
			messagebox.showerror(title='Pipette selection', message='You must have a p20 pipette to use this protocole.')

	def change_nb_aliquots_limit(self, val):
		self.nb_aliquots.config(to=plate_dict[val][0]*plate_dict[val][1])
		return None

	def create_aliquots_script(self):
		try:
			if self.spot_jobname_var.get() == '' or self.spot_jobname_var.get() == 'opentron_app':
				raise ValueError
			self.write_aliquots_script()
		except:
			messagebox.showerror(title='Job name', message='Please enter a job name')

	def write_aliquots_script(self):
		script_file = open('./Scripts/aliquoting.py', 'r')
		script = script_file.readlines()
		script_file.close()
		script_name = self.spot_jobname_var.get().replace(' ', '_')
		script_file = open('%s.py' % (script_name), 'w')
		if self.left_pipette_var.get() == 'p20_single_gen2':
			script[34] = "\tp20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks=tip_rack)\n"
		elif self.right_pipette_var.get() == 'p20_single_gen2':
			script[34] = "\tp20 = protocol.load_instrument('p20_single_gen2', 'right', tip_racks=tip_rack)\n"
		script[28] = "\tsource_plate = protocol.load_labware('%s', 1)\n" % (self.source_plate_var.get())
		script[29] = "\tdest_plate = protocol.load_labware('%s', 2)\n" % (self.dest_plate_var.get())
		self.well_list = self.generate_well_list(plate_dict[self.dest_plate_var.get()][0], plate_dict[self.dest_plate_var.get()][1])
		script[24] = "\twells = %s\n" % (self.well_list)
		source_wells = self.check_wells_list()
		print(source_wells)
		if source_wells != False:
			print('here')
			script[22] = '\tsource_wells = %s\n' % (source_wells)
			script[23] = '\tnb_dest_wells = [%i]\n' % (self.nb_aliquots.get())
			script[25] = '\tvol = [%i]\n' % (self.volume.get())
			script_file.writelines(script)
			script_file.close()
			messagebox.showinfo(title='Resulting script', message='\tYour source plate must be on slot 1 and your dest plate must on slot 2.\n\n\tYour script is available here:\n%s\%s.py' %(main_path, script_name))
		return None

	def generate_well_list(self, x, y):
		alphabet = string.ascii_uppercase
		well_list = list()
		for i in range(x):
			for j in range(y):
				well_list.append(alphabet[j]+str(i+1))
		return well_list

	def check_wells_list(self):
		wells = re.findall(r"\w\d", self.source_well_var.get())
		#wells = self.source_well_var.get().split(',')
		print(wells)
		correct_wells = list()
		if len(wells) > 0:
			for i in wells:
				print(i)
				if i.strip() in self.well_list:
					correct_wells.append(i.strip())
					return correct_wells
				else:
					messagebox.showerror(title='Uncorrect well ID', message='This well ID does not exist: %s.\nPlease enter a valid well ID.' % (i.strip()))
					return False
		else:
			messagebox.showerror(title='Uncorrect well ID', message='Please enter the source well ID.')
			return False			
def main():
	app_interface() 
	
if __name__ == "__main__":
	main()