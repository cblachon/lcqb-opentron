#from opentrons.drivers.rpi_drivers import gpio
import json
import os
import datetime
import time
# metadata
from opentrons import protocol_api

metadata = {'apiLevel': '2.8'}

## Sorry about this! You are totally right the code below doesn't work with protocol. It only works with ctx 
def run(ctx):
    protocol = ctx
    ctx._hw_manager.hardware._backend.gpio_chardev.set_rail_lights(red=True)
    tr2 = protocol.load_labware('opentrons_96_tiprack_300ul', '1')
    p300_single = protocol.load_instrument('p300_single_gen2', 'left', tip_racks=[tr2])
    reservoir = protocol.load_labware('nest_12_reservoir_15ml', location='2')
    p300_single.pick_up_tip()
    ctx._hw_manager.hardware._backend.gpio_chardev.set_rail_lights(green=True)
    #ctx._hw_manager.hardware._backend.gpio_chardev.set_button_light(blue=True)
    #ctx._implementation.get_hardware()._backend.gpio_chardev.set_button_light(green=True) -> Change after 4.1
    p300_single.aspirate(100, reservoir.wells()[0])
    p300_single.return_tip()
    
