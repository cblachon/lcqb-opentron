This tool has been developed to generate scripts compatible with the OpenTron2.

1. Introduction
	This application has been developed in order to generate scripts compatible with the OpenTron2. Some features are required to use it:
		- Python 3 and several modules (Tkinter) required to make this application work and to generate the scripts.
		- The OpenTron application (https://opentrons.com/ot-app/) required to run the protocol generated.

	When this application is launched, there is the home page used to precise how the pipettes are set up on the robot: what kind of pipettes on which side. After precising this, you can chose what protocol you want to generate.
	Now, 4 protocols are available: MoClo assemblies, transformation, spotting and aliquots. When you chose one of those protocols, a check up occurs to see if the required pipettes are available to generate this protocol.

2. Protocols
	First of all, for all the protocol, the first thing to specify is the job name you want to use. This job name correspond to the name of your final script, for example if you enter 'protocol_01' your resulting script will be named protocol_01.py.

	a) MoClo assemblies
		About the specific features to complete for the MoClo assemblies you have to select your source plate format*, and the temperature cycle to apply. Although it is recommended to perform this on an outer thermocycler it is possible to use the OpenTron temperature module to do it. However, we should warn you that it might affect the efficiency of the assemblies.

		When both those parameters are selected, the next step is to complete the "Ligation MoClo" Excel file to describe your designs. You must be careful to remove useless sheets and to duplicate one sheet if you want to perform multiple assemblies. Moreover, it is very important that the file structure stays unchanged otherwise the data parsing will be troubled. You can easily download this file on your current directory if you don't have it with the download template button. When your file has been completed you can select the Input Data button which will open a file explorateur page so you can select your input file. This will generate your script in your current directory, and a small reminder of the position of your liquids appears.		

	b) Transformation + spotting
		To generate transformation protocol, you enter some informations in the app:
			- Number of transformations: until 24 transformations are feasible at a time. However, if you are not doing 8, 16 or 24 transformations you will waste some LB and bacterias because of the multi-channel.
			- The well-plate format containing your DNA (24, 96 or 384 wells)
			- Whether or not you want the OT2 to perform the spotting step for you. If you do:
			- Number of dilutions you want to do for each transformations
			- The dilution factor
			- Describe the volume you want to transfer in which well from your source plate. A scheme in which you can enter your volume is available. A validation occurs on keychanges, your volume must be a float or an integer between 1 and 20, otherwise nothing . 

		When those parameters are well described you can generate your scipt with the 'Generate script' button. This will write your script in the directory you are located in.

3. Help
	* If your plate is not available in the plate list it is possible to add it as long as it respects the SBS format. 
	To do so, just contact Clémence Blachon (clemence.blachon@sorbonne-universite.fr).